package Main;

import Model.*;

import java.time.LocalDate;
import java.util.Scanner;

public class ShoppingCart {
    public static void main(String[] args) {

        int orderNumber = 1;

        ProductCategory makanan = ProductCategory.FOOD;
        ProductCategory minuman = ProductCategory.DRINK;
        ProductCategory sabun = ProductCategory.SOAP;

        StatusOrder delivered = StatusOrder.DELIVERED;
        StatusOrder ordered = StatusOrder.ORDERED;

        Scanner masukan = new Scanner(System.in);

        PaymentStatus paid = PaymentStatus.PAID;
        PaymentStatus unpaid = PaymentStatus.UNPAID;

        Order order = new Order();

        Cashier cashier1 = new Cashier("Arif");

        Product makanan1 = new Product(
                "SilverQueen",
                "SQ123",
                "Silverqueen adalah cokelat terenak",
                2000,
                "images/makanan/silverqueen.jpg",
                LocalDate.of(2022,12,30),
                makanan
        );

        Product makanan2 = new Product(
                "Chitato",
                "CH123",
                "Chitato adalah snack micin terlezat",
                1000,
                "images/makanan/chitato.jpg",
                LocalDate.of(2021,1,12),
                makanan
        );

        Product minuman1 = new Product(
                "Coca-Cola",
                "CC123",
                "Coca Cola adalah minuman soda termantap",
                500,
                "images/minuman/coca-cola.jpg",
                LocalDate.of(2020, 1, 6),
                minuman
        );

        Product sabun1 = new Product(
                "Detol",
                "DT123",
                "Detol adalah sabun paling higienis",
                300,
                "images/sabun/detol.jpg",
                LocalDate.of(2020,1,3),
                sabun
        );

        //display menu
        displayMenu();
        int menu = masukan.nextInt();

        switch (menu) {
            case 1:
                //display product
                System.out.println("\nDaftar Product");
                System.out.println("1. " + makanan1.getName());
                System.out.println("2. " + makanan2.getName());
                System.out.println("3. " + minuman1.getName());
                System.out.println("4. " + sabun1.getName());
                System.out.print("Masukan: ");
                int beli = masukan.nextInt();;

                switch (beli){
                    case 1:
                        order(order, makanan1, cashier1, orderNumber, ordered, );
                        orderNumber++;
                }

        }




    } public static void displayMenu(){
        System.out.println("Selamat datang di Moins Store");
        System.out.println("Menu: ");
        System.out.println("1. Beli");
        System.out.println("2. Bayar");
        System.out.println("3. Keranjang");
        System.out.println("4. Riwayat Belanja");
        System.out.print("Masukan: ");
    }

    public static void order(
            Order order,
            Product product,
            Cashier cashier,
            int orderNumber,
            StatusOrder statusOrder
    ){
        order.setOrderDate(LocalDate.now());
        order.setCashier(cashier);
        order.setOrderNumber(orderNumber);
        order.setStatusOrder(statusOrder);


    }
}
