package Model;

public interface Tax {
    double TAX_FOOD = 0.05;
    double TAX_DRINK = 0.1;
    double TAX_SOAP = 0.07;
}
