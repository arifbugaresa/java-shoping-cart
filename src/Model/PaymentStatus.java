package Model;

public enum PaymentStatus {
    UNPAID, PAID;
}
